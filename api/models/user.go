package models

type User struct {
	Id    uint
	Name  string
	Email string `json:"email"`
	Pass  string `json:"password"`
}

func (user *User) Map() map[string]interface{} {
	return map[string]interface{}{"id": user.Id, "name": user.Name, "email": user.Email}
}
