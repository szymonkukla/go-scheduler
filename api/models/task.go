package models

type Task struct {
	Id          uint   `json:"id"`
	UserId      uint   `json:"userId"`
	Date        uint   `json:"date"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Done        bool   `json:"done"`

	GroupId int `json:"groupId"`
}

func (task *Task) Map() map[string]interface{} {
	return map[string]interface{}{
		"id":          task.Id,
		"userId":      task.UserId,
		"date":        task.Date,
		"title":       task.Title,
		"description": task.Description,
		"groupId":     task.GroupId,
	}
}

type TaskDelete struct {
	UserId uint `json: userId`
	TaskId uint `json: taskId`
}
