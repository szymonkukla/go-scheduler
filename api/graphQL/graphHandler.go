package graphQL

import (
	"encoding/json"
	"fmt"

	"github.com/graphql-go/graphql"
)

func GetTasks(query string) (*[]byte, bool) {
	taskQuery := graphql.ObjectConfig{Name: "task", Fields: TaskFields}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(taskQuery)}
	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		fmt.Errorf("failed to create new schema, error: %v", err)
	}

	params := graphql.Params{Schema: schema, RequestString: query}
	r := graphql.Do(params)
	if len(r.Errors) > 0 {
		fmt.Errorf("Bad graphQL query: ", err)
		return nil, false
	}
	res, err := json.Marshal(r)
	return &res, err == nil
}
