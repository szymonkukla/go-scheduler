package graphQL

import (
	r "go-scheduler/repositories"
	"strconv"

	"github.com/graphql-go/graphql"
)

var TaskFields = graphql.Fields{
	"task": &graphql.Field{
		Type:        Task,
		Description: "Get task By Id",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			id, ok := p.Args["id"].(int)
			if ok {
				return r.GetTaskById(uint(id)), nil
			}
			return nil, nil
		},
	},
	"tasks": &graphql.Field{
		Type:        graphql.NewList(Task),
		Description: "Get tasks By userId and date frame",
		Args: graphql.FieldConfigArgument{
			"userId": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
			"dateFrom": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"dateTo": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"done": &graphql.ArgumentConfig{
				Type: graphql.Boolean,
			},
		},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			userId, ok := p.Args["userId"].(int)
			dateFromArg, ok := p.Args["dateFrom"].(string)
			dateFrom, _ := strconv.ParseUint(dateFromArg, 10, 64)
			dateToArg, _ := p.Args["dateTo"].(string)
			dateTo, _ := strconv.ParseUint(dateToArg, 10, 64)
			doneArg, hasDone := p.Args["done"].(bool)
			if ok {
				if hasDone {
					return r.GetTasksByDates(uint(userId), uint(dateFrom), uint(dateTo), &doneArg), nil
				} else {
					return r.GetTasksByDates(uint(userId), uint(dateFrom), uint(dateTo), nil), nil
				}
			}
			return nil, nil
		},
	},
}
