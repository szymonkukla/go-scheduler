module go-scheduler

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/graphql-go/graphql v0.7.8
	google.golang.org/genproto v0.0.0-20190911173649-1774047e7e51 // indirect
)
