package controllers

import (
	"encoding/json"
	"fmt"
	ql "go-scheduler/graphQL"
	h "go-scheduler/httpUtils"
	m "go-scheduler/models"
	r "go-scheduler/repositories"
	"io/ioutil"
	"net/http"
)

func GetTasks(w http.ResponseWriter, req *http.Request) {
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	resp, status := ql.GetTasks(string(reqBody))

	if status {
		h.RespondStr(w, resp, 200)
	} else {
		h.Respond(w, h.Message(false, "Invalid graphQl query!"), 401)
	}
}

func CreateTask(w http.ResponseWriter, req *http.Request) {
	var task m.Task
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	err = json.Unmarshal(reqBody, &task)

	if err == nil {
		r.CreateTask(&task)
		h.Respond(w, h.Message(true, "Task Created"), 200)
	} else {
		fmt.Println(err)
		h.Respond(w, h.Message(false, "Invalid request"), 400)
	}
}

func EditTask(w http.ResponseWriter, req *http.Request) {
	var task m.Task
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	err = json.Unmarshal(reqBody, &task)

	if err == nil {
		r.EditTask(&task)
		h.Respond(w, h.Message(true, "Task Created"), 200)
	} else {
		fmt.Println(err)
		h.Respond(w, h.Message(false, "Invalid request"), 400)
	}
}

func RemoveTask(w http.ResponseWriter, req *http.Request) {
	var task m.TaskDelete
	reqBody, err := ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Fprintf(w, "Kindly enter data with the event title and description only in order to update")
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	err = json.Unmarshal(reqBody, &task)

	if err == nil {
		r.RemoveTask(task.UserId, task.TaskId)
		h.Respond(w, h.Message(true, "Task deleted"), 200)
	} else {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
	}
}
