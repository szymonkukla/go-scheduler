package controllers

import (
	"encoding/json"
	h "go-scheduler/httpUtils"
	m "go-scheduler/models"
	r "go-scheduler/repositories"
	"net/http"
)

func Login(w http.ResponseWriter, req *http.Request) {
	user := m.User{}
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	resp := r.Login(user)

	if resp != nil {
		h.Respond(w, resp.Map(), 200)
	} else {
		h.Respond(w, h.Message(false, "Invalid login or password"), 401)
	}
}

func CreateUser(w http.ResponseWriter, req *http.Request) {
	user := m.User{}
	err := json.NewDecoder(req.Body).Decode(&user)

	if err != nil {
		h.Respond(w, h.Message(false, "Invalid request"), 400)
		return
	}

	if r.CreateUser(&user) {
		h.Respond(w, h.Message(false, "User created"), 200)
	} else {
		h.Respond(w, h.Message(false, "User exist!"), 409)
	}

}
