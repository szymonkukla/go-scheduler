package main

import (
	"fmt"
	"go-scheduler/controllers"
	h "go-scheduler/httpUtils"
	"go-scheduler/models"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.Methods("OPTIONS").HandlerFunc(h.CorsHandler)
	router.HandleFunc("/login", controllers.Login).Methods("POST")
	router.HandleFunc("/login/create", controllers.CreateUser).Methods("POST")
	router.HandleFunc("/tasks", controllers.GetTasks).Methods("POST")
	router.HandleFunc("/tasks/create", controllers.CreateTask).Methods("POST")
	router.HandleFunc("/tasks/edit", controllers.EditTask).Methods("POST")
	router.HandleFunc("/tasks/delete", controllers.RemoveTask).Methods("POST")

	fmt.Println("Go-Scheduler port: 8080")
	err := http.ListenAndServe(":8080", router)
	log.Fatal(err)
}

func Test() string {
	var u = models.User{Id: 1, Name: "Hello, world."}
	return u.Name
}
