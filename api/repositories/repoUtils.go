package repositories

func compareDates(d1, d2 uint) bool {
	day := (d1 / 10000) * 10000
	return day <= d2 && day+2359 >= d2
}

func onDateRange(d1, d2, date uint) bool {
	if d1 == 0 || d2 == 0 {
		return false
	}
	_d1 := (d1 / 10000) * 10000
	_d2 := ((d2 / 10000) * 10000) + 2359

	return _d1 <= date && _d2 >= date
}
