package repositories

import (
	"fmt"
	m "go-scheduler/models"
)

func GetTaskById(taskId uint) *m.Task {
	fmt.Println("getting task by id: ", taskId)
	for _, t := range Tasks {
		if t.Id == taskId {
			return &t
		}
	}
	return nil
}

func GetTasksByDates(userId, dateFrom, dateTo uint, done *bool) *[]m.Task {
	var result = []m.Task{}

	for _, t := range Tasks {
		if t.UserId == userId && (compareDates(dateFrom, t.Date) || onDateRange(dateFrom, dateTo, t.Date)) && (done == nil || t.Done == *done) {
			result = append(result, t)
		}
	}

	return &result
}

func CreateTask(task *m.Task) {
	tLen := len(Tasks)
	if tLen == 0 {
		task.Id = 0
	} else {
		task.Id = uint(Tasks[tLen-1].Id + 1)
	}

	Tasks = append(Tasks, *task)
}

func EditTask(task *m.Task) {
	for _, t := range Tasks {
		if t.UserId == task.UserId && t.Id == task.Id {
			t.Date = task.Date
			t.Title = task.Title
			t.Description = task.Description
			t.Done = task.Done
			return
		}
	}
}

func RemoveTask(userId, taskId uint) bool {
	rem := false
	for i, t := range Tasks {
		if t.UserId == userId && t.Id == taskId {
			Tasks = append(Tasks[:i], Tasks[i+1:]...)
			rem = true
		}
	}
	return rem
}
