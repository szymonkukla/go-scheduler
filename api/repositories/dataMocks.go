package repositories

import (
	m "go-scheduler/models"
)

var Users = []m.User{
	m.User{
		Id:    0,
		Name:  "Roman",
		Email: "r@r.com",
		Pass:  "roman",
	},
	m.User{
		Id:    1,
		Name:  "Karyna",
		Email: "k@k.com",
		Pass:  "karyna",
	}}

var Tasks = []m.Task{
	m.Task{
		Id:          0,
		UserId:      0,
		Date:        201909121750,
		Title:       "Zakupy",
		Description: "Kup pomidory",
		GroupId:     0,
	},
	m.Task{
		Id:          1,
		UserId:      0,
		Date:        201909121300,
		Title:       "Spotkanie z Panem Derektorem",
		Description: "Pogadaj z Krzysztofem Jarzyną",
		GroupId:     0,
	},
	m.Task{
		Id:          2,
		UserId:      0,
		Date:        201909130800,
		Title:       "Piątek!",
		Description: "Piątek, piąteczek...",
		Done:        true,
		GroupId:     0,
	},
	m.Task{
		Id:          3,
		UserId:      0,
		Date:        201909110900,
		Title:       "Mechanik",
		Description: "Oddaj auto do mechanika",
		GroupId:     0,
	},
	m.Task{
		Id:          4,
		UserId:      0,
		Date:        201909091200,
		Title:       "Krystyna z gazowni",
		Description: "Zapłać za gaz...",
		GroupId:     0,
	},
}
