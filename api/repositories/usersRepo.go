package repositories

import (
	m "go-scheduler/models"
	"strings"
)

func Login(user m.User) *m.User {
	for _, u := range Users {
		if strings.ToLower(u.Email) == strings.ToLower(user.Email) && u.Pass == user.Pass {
			return &u
		}
	}
	return nil
}

func CreateUser(user *m.User) bool {
	for _, u := range Users {
		if strings.ToLower(u.Email) == strings.ToLower(user.Email) {
			return false
		}
	}

	tLen := len(Tasks)
	if tLen == 0 {
		user.Id = 0
	} else {
		user.Id = uint(Tasks[tLen-1].Id + 1)
	}

	Users = append(Users, *user)
	return true
}
