import React from 'react';
import { Row, Accordion, Card, Form, FormCheck, Button, Col, ButtonGroup } from 'react-bootstrap';
import './task.css';
import { ITask } from '../../models/ITask';
import RemoveModal from '../removeModal/removeModal';

export interface TaskProps {
  task: ITask;
  remove: (taskId: number) => void;
  edit: (task: ITask) => void;
}

interface TaskState {
  deleting: boolean;
}

export default class Task extends React.Component<TaskProps, TaskState> {
  /**
   *
   */
  constructor(p: TaskProps, s: TaskState) {
    super(p, s);
    this.state ={ 
      deleting: false
    }
  }

  private remove(s: boolean) {
    this.setState({deleting: s});
    if (s && this.props.task.id != undefined) {
      this.props.remove(this.props.task.id)
    }
  }

  public render() {
    return (
      <div>
        <Accordion>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="0">
              <Row>
                <Col>
                  <input type="checkbox" defaultChecked={this.props.task.done} />
                  <span className="task-title">{this.props.task.title}</span>
                </Col>
                <Col xs="auto">
                  <ButtonGroup>
                    <Button variant="secondary" size="sm" onClick={() => this.props.edit(this.props.task)}>
                      Edytuj
                    </Button>
                    <Button variant="danger" size="sm" onClick={() => this.setState({deleting: true})}>
                      Usuń
                    </Button>
                  </ButtonGroup>
                </Col>
              </Row>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Card.Body>{this.props.task.description}</Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        {this.state.deleting ? <RemoveModal choice={(s) => this.remove(s)}></RemoveModal> : ''}
      </div>
    );
  }
}
