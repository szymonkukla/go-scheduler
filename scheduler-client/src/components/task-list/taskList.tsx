import React from 'react';
import { ListGroup, Container, ListGroupItem, Row, Col, Badge, Navbar, Button } from 'react-bootstrap';
import { ITask } from '../../models/ITask';
import DateUtils from '../../utils/dateUtils';
import Task from '../task/task';
import './taskList.css';
import { TaskHandler } from '../../httpClient/taskHandler';
import CrateTaskModal from '../taskCreateModal/taskCreateModal';

interface TaskListState {
  dates: {
    date: Date;
    tasks: ITask[];
  }[];
  isCreateEdit: boolean;
  createDate?: Date;
  editTask?: ITask;
}

export default class TaskList extends React.Component<any, TaskListState> {
  /**
   *
   */
  private start?: string = new Date(new Date().setDate(new Date().getDate() - 7)).toISOString().substr(0, 10);
  private end?: string = new Date(new Date().setDate(new Date().getDate() + 3)).toISOString().substr(0, 10);
  constructor(p: any, s: TaskListState) {
    super(p, s);

    this.state = {
      dates: [],
      isCreateEdit: false
    };
    this.getTasks();
  }

  private getTasks() {
    let _start = DateUtils.dateFromString(this.start) || new Date();
    let _end = DateUtils.dateFromString(this.end) || new Date();
    TaskHandler.getTasks(_start, _end).then(r => {
      this.setCallendarTasks(_start, _end, r.data.tasks);
    });
  }

  private removeTask(taskId: number) {
    TaskHandler.removeTask(taskId)
      .then(r => {
        if (r.status) {
          this.getTasks();
        }
      })
      .catch(c => alert('Nie można usunąć zadania!'));
  }

  private setCallendarTasks(dFrom: Date, dTo: Date, tasks: ITask[]) {
    let date = dTo;
    let days = [];

    while (date > dFrom) {
      const _tasks = tasks.length ? tasks.filter(t => DateUtils.comapreDate(DateUtils.dateFromKey(t.date), date)) : [];
      days.push({
        date: new Date(date),
        tasks: _tasks
      });
      date.setDate(date.getDate() - 1);
    }
    this.setState({ dates: days, isCreateEdit: false });
  }

  private showCreateModal(state: boolean, date?: Date, task?: ITask) {
    this.setState({createDate: date, editTask: task, isCreateEdit: state});
  }

  private createTask(task: ITask) {
    TaskHandler.createTask(task).then(r => this.getTasks());
  }

  private editTask(task: ITask) {
    TaskHandler.createTask(task).then(r => this.getTasks());
  }

  public render() {
    return (
      <Col xs={12}>
        <div className="list-toolbar">
          <Row>
            <Col xs={4}>
              <input className="form-control" type="date" defaultValue={this.start} onChange={v => (this.start = v.target.value)} />
            </Col>
            <Col xs={4}>
              <input className="form-control" type="date" defaultValue={this.end} onChange={v => (this.end = v.target.value)} />
            </Col>
            <Col xs={4}>
              <Button className="btn btn-success" onClick={() => this.getTasks()}>
                Szukaj
              </Button>
            </Col>
          </Row>
        </div>
        <Row>
          <Col xs={12}>
            <ListGroup variant="flush">
              {this.state.dates.map((el, i) => (
                <ListGroupItem key={i} variant={DateUtils.comapreDate(new Date(), el.date) ? 'secondary' : el.date.getDay() === 0 || el.date.getDay() === 6 ? 'info' : undefined}>
                  <Row>
                    <Col>
                      <div className="date-header">
                        <strong>{DateUtils.getDayString(el.date)}</strong> <span>{DateUtils.getDateString(el.date)}</span>
                      </div>
                    </Col>
                    <Col xs="auto">
                        <Button variant="success" size="sm" onClick={() => this.showCreateModal(true, el.date)}>
                          Dodaj
                        </Button>
                    </Col>
                  </Row>
                  <hr></hr>
                  {el.tasks.length ? el.tasks.map((task, ti) => <Task task={task} key={i + 'task' + ti} remove={(id: number) => this.removeTask(id)} edit={((t: ITask) => this.showCreateModal(true, undefined, t))}></Task>) : <div>Brak zadań</div>}
                </ListGroupItem>
              ))}
            </ListGroup>
          </Col>
        </Row>
        {this.state.isCreateEdit ? <CrateTaskModal onHide={() => this.setState({isCreateEdit: false})} onCreate={(t) => this.createTask(t)} onEdit={(t) => this.editTask(t)} date={this.state.createDate} editTask={this.state.editTask}></CrateTaskModal> : ''}
      </Col>
    );
  }
}
