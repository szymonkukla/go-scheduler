import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import './login.css';
import { IUser } from '../../models/IUser';
import HttpClient from '../../httpClient/httpClient';
import { Alert, Row, Col } from 'react-bootstrap';

export interface LoginProps {
  onLogged: (user: IUser) => void;
}
export interface LoginState {
  badCred: boolean;
}

export default class SignInPage extends React.Component<LoginProps, LoginState> {
  private classes: any;
  private _email = '';
  private _pass = '';
  /**
   *
   */
  constructor(p: LoginProps, c: any) {
    super(p, c);
    this.state = {
      badCred: false
    };

    this.classes = makeStyles(theme => ({
      global: {
        backgroundColor: theme.palette.common.white
      },
      paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
      avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
      },
      form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
      },
      submit: {
        margin: theme.spacing(3, 0, 2)
      }
    }));
  }

  private BadCred() {
    let st = this.state as any;
    if (st.badCred) {
      if (!this._email) {
        return <div className="bad-cred">Wpisz email i hasło!</div>;
      }
      if (!this._pass) {
        return <div className="bad-cred">Podaj hasło!</div>;
      }
      return <div className="bad-cred">Błędny email lub hasło!</div>;
    }
  }

  private signInRequest = () => {
    if (this._email && this._pass) {
      HttpClient.$.post('login', { email: this._email, password: this._pass })
        .then(v => {
          this.setState({ badCred: false });
          this.props.onLogged(v);
        })
        .catch(r => {
          this.setState({ badCred: true });
        });
    } else {
      this.setState({ badCred: true });
    }
  };

  public render() {
    return (
      <Container className={this.classes.global} component="main" maxWidth="xs">
        <br />
        <Row>
          <Col>
            <Alert show={true} variant="success">
              <Alert.Heading>Witamy w GO! Scheduler</Alert.Heading>
              <hr />
              <p>Autorzy:</p>
              <p>Szymon Kukla, Rafał Sygut, Maciej Wojcieszyk, Marcin Kloc</p>
            </Alert>
          </Col>
        </Row>
        <Row>
        <Col>
          <form className={this.classes.form} noValidate>
            <TextField variant="outlined" margin="normal" required fullWidth id="email" label="Email" name="email" autoComplete="email" autoFocus onChange={val => (this._email = val.target.value)} />
            <TextField variant="outlined" margin="normal" required fullWidth name="password" label="Hasło" type="password" id="password" autoComplete="current-password" onChange={val => (this._pass = val.target.value)} />
            {this.state.badCred}
            {this.BadCred()}
            <Button fullWidth variant="contained" color="primary" className={this.classes.submit} onClick={this.signInRequest}>
              Zaloguj
            </Button>
          </form>
          </Col>
        </Row>
      </Container>
    );
  }
}
