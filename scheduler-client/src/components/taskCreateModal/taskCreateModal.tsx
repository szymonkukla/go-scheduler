import React from 'react';
import { ITask } from '../../models/ITask';
import { Modal, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap';
import { SessionHandler } from '../../sessionHandler';
import DateUtils from '../../utils/dateUtils';

interface ICreateModalProps {
  editTask?: ITask;
  date?: Date;
  onCreate: (task: ITask) => void;
  onEdit: (task: ITask) => void;
  onHide: () => void;
}

interface ICreateState {
    valid: boolean;
}

export default class CrateTaskModal extends React.Component<ICreateModalProps, ICreateState> {
    /**
     *
     */
    constructor(p: ICreateModalProps, s: ICreateState) {
        super(p,s);
        this.state={
            valid: true
        }        
    }
    private currentTask: ITask = this.props.editTask ? this.props.editTask : {
        userId: 0, 
        date: DateUtils.dateToDateKey(this.props.date ? this.props.date : new Date()),
        title: '',
        description: '',
        done: false
    }

    public setDate(ds: string) {
        const d = DateUtils.dateFromString(ds);
        const cd = DateUtils.dateFromKey(this.currentTask.date);
        if (d) {
            d.setHours(cd.getHours());
            d.setMinutes(cd.getMinutes());
            this.currentTask.date = DateUtils.dateToDateKey(d);
        }
    }

    public setTime(ts: string) {
        const d = ts.split(':').map(t => Number(t));
        const cd = DateUtils.dateFromKey(this.currentTask.date);
        if (cd) {
            cd.setHours(d[0]);
            cd.setMinutes(d[1]);
            this.currentTask.date = DateUtils.dateToDateKey(cd);
        }
    }

    private getDate() {
        const d = this.currentTask.date ? DateUtils.dateFromKey(this.currentTask.date) : new Date();
        function pad(num: number) {
            let s = num+"";
            while (s.length < 2) s = "0" + s;
            return s;
        }
        return `${d.getFullYear()}-${pad(d.getMonth()+1)}-${pad(d.getDate())}`;
    }

    private getTime() {
        const d = this.currentTask.date ? DateUtils.dateFromKey(this.currentTask.date) : new Date();
        function pad(num: number) {
            let s = num+"";
            while (s.length < 2) s = "0" + s;
            return s;
        }
        return `${pad(d.getHours())}:${pad(d.getMinutes())}`;
    }

    private Create() {
        if (this.currentTask.title && DateUtils.dateFromKey(this.currentTask.date)) {
            if (this.props.editTask) {
                this.props.onEdit(this.currentTask);
            } else {
                this.props.onCreate(this.currentTask);
            }
        } else {
            this.setState({valid: false});
        }
    }

  public render() {
    return (
      <Modal show={true} onHide={() => this.props.onHide()} aria-labelledby="example-modal-sizes-title-sm">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">{this.props.editTask ? 'Edycja zadania' : 'Nowe zadanie'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col xs="12">
              <InputGroup className="mb-5">
                <InputGroup.Prepend>
                  <InputGroup.Text id="date-inp">Data</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl aria-label="Default" aria-describedby="date-inp" type="date" defaultValue={this.getDate()} onChange={(c: any) => this.setDate(c.target.value)} isInvalid={!this.state.valid}/>
                <InputGroup.Prepend>
                  <InputGroup.Text id="time-inp">Godzina</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl aria-label="Default" aria-describedby="time-inp" type="time" defaultValue={this.getTime()} onChange={(c: any) => this.setTime(c.target.value)} isInvalid={!this.state.valid}/>
              </InputGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form>
                <Form.Group>
                  <Form.Label>Tytuł</Form.Label>
                  <Form.Control type="text" placeholder="Tytuł" defaultValue={this.currentTask.title} onChange={(c: any,) => this.currentTask.title = c.target.value} isInvalid={!this.state.valid}/>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Opis</Form.Label>
                  <Form.Control as="textarea" rows="3" defaultValue={this.currentTask.description} onChange={(c: any) => this.currentTask.description = c.target.value} />
                </Form.Group>
                <Form.Check type="checkbox" label="Wykonano" defaultChecked={this.currentTask.done} onChange={() => this.currentTask.done = !this.currentTask.done}/>
              </Form>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={() => this.Create()}>
            {this.props.editTask ? 'Zapisz' : 'Utwórz'}
          </Button>
          <Button variant="danger" onClick={() => this.props.onHide()}>
            Anuluj
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
