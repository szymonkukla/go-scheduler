import React from 'react';
import { Modal, Button } from 'react-bootstrap';

interface IRemoveModalProps {
  choice: (state: boolean) => void;
}

export default class RemoveModal extends React.Component<IRemoveModalProps, any> {
  public render() {
    return (
      <Modal show={true} onHide={() => this.props.choice(false)} aria-labelledby="example-modal-sizes-title-sm">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">Usuwanie zadania</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        Czy na pewno chcesz usunąć to zadanie?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={() => this.props.choice(true)}>
            Usuń
          </Button>
          <Button variant="danger" onClick={() => this.props.choice(false)}>
            Anuluj
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
