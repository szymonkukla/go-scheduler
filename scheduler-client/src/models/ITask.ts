export interface ITask {
    id?: number;
    userId: number; 
    date: string;
    title: string;
    description: string;
    done?: boolean;
}