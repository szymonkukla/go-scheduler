import React from 'react';
import SignInPage from './components/login-page/login';
import { IUser } from './models/IUser';
import TaskList from './components/task-list/taskList';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Navbar, Container, Form, FormControl, Button } from 'react-bootstrap';
import { SessionHandler } from './sessionHandler';

interface AppState {
  currentUser?: IUser;
}

class App extends React.Component<any, AppState> {
  /**
   *
   */
  constructor(p: any, s: AppState) {
    super(p, s);
    this.state = { currentUser: SessionHandler.getUser()}
  }
  private onLogged = (user: IUser) => {
    SessionHandler.login(user);
    this.checkSession();
  };

  public logout() {
    SessionHandler.logout();
    this.checkSession();
  }

  public checkSession() {
    this.setState({ currentUser: SessionHandler.getUser() });
  }

  private appContent() {
    if (!this.state || !this.state.currentUser) {
      return <SignInPage onLogged={this.onLogged}></SignInPage>;
    } else {
      return (
        <div>
          <Navbar className="nav-bar" bg="dark" variant="dark" expand="lg">
            <Navbar.Brand href="#home">
              <img alt="" src="/logo.svg" width="30" height="30" className="d-inline-block align-top" />
              GO! Scheduler
            </Navbar.Brand>
            <Button className="logout-btn" variant="outline-light" onClick={() => this.logout()}>
              Wyloguj
            </Button>
          </Navbar>
          <div className="content">
            <Container>
              <TaskList></TaskList>
            </Container>
          </div>
        </div>
      );
    }
  }

  public render() {
    return <div className="App">{this.appContent()}</div>;
  }
}

export default App;
