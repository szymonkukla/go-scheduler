export default class DateUtils {
    public static comapreDate(d1: Date, d2: Date) {
        if (!d1 || !d2) {
            return false;
        }

        let _d1 = new Date(d1.getTime());
        _d1.setHours(0,0,0,0);
        let _d2 = new Date(d2.getTime());
        _d2.setHours(0,0,0,0);

        return _d1.getTime() === _d2.getTime();
    }

    public static checkDateRange(from: Date, to?: Date, date?: Date) {
        if (!from || !to || !date) {
            return false;
        }

        let _d1 = new Date(from.getTime());
        _d1.setHours(0,0,0,0);
        let _d2 = new Date(to.getTime());
        _d2.setHours(23,59,59,999);

        return _d1.getTime() <= date.getTime() && _d2.getTime() >= date.getTime();
    }

    public static dateFromString(date?: string) {
        if (!date) {
            return undefined;
        }
        const d = date.split('-').map(v => Number(v));
        return new Date(d[0],d[1]-1,d[2]);
    }

    public static getDateString(d: Date) {
        return `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`
    }

    public static getDayString(d: Date) {
        const days = [
            'Niedziela','Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'
        ];
        return days[d.getDay()];
    }

    public static dateToDateKey(d: Date) {
        function pad(num: number) {
            let s = num+"";
            while (s.length < 2) s = "0" + s;
            return s;
        }
        return `${d.getFullYear()}${pad(d.getMonth() + 1)}${pad(d.getDate())}${pad(d.getHours())}${pad(d.getMinutes())}`;
    }

    public static dateFromKey(dKey: string) {
        const y = Number(dKey.substr(0,4));
        const m = Number(dKey.substr(4,2)) - 1;
        const d = Number(dKey.substr(6,2));
        const hh = Number(dKey.substr(8,2));
        const mm = Number(dKey.substr(10,2));
        return new Date(y,m,d,hh,mm);
    }
}