import { IUser } from './models/IUser';

const user: IUser = {
    name: 'Roman',
    email: 'r@r.com',
    id: 0
  };

export class SessionHandler {
    private static currentUser?: IUser = user;

    public static login(user: IUser) {
        this.currentUser = user;
    }

    public static logout() {
        this.currentUser = undefined;
    }

    public static getUser() {
        return this.currentUser;
    }
}