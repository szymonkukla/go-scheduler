import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export default class HttpClient {
  //////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////  SINGLETON INSTANCE CONSTRUCTOR  /////////////////////////////

  private static __self_instance: HttpClient;
  static get $() {
    if (!this.__self_instance) {
      this.__self_instance = new HttpClient();
    }
    return this.__self_instance;
  }

  private constructor() {}
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////
  private api = this.initAxios();

  public onUnauthorised?: () => void;
  public onNotPermited?: () => void;
  public onNotFound?: () => void;

  public async post<T = any>(apiPath: string, data: any): Promise<T> {
    return new Promise((res, rej) => {
      this.api
        .post(apiPath, data)
        .then(r => {
          res(this.parseResponse(r));
        })
        .catch(err => {
          this.errorHandler(err, rej);
        });
    });
  }

  public async get<T = any>(apiPath: string, parameters?: object): Promise<T> {
    const params: AxiosRequestConfig = {
      params: parameters
    };
    return new Promise((res, rej) => {
      this.api
        .get(apiPath, params)
        .then(r => {
          res(this.parseResponse(r));
        })
        .catch(err => {
          this.errorHandler(err, rej);
        });
    });
  }

  public async delete<T = any>(apiPath: string, parameters?: object): Promise<T> {
    const params: AxiosRequestConfig = {
      params: parameters
    };
    return new Promise((res, rej) => {
      this.api
        .delete(apiPath, params)
        .then(r => {
          res(this.parseResponse(r));
        })
        .catch(err => {
          this.errorHandler(err, rej);
        });
    });
  }

  private parseResponse(resp: AxiosResponse) {
    if (resp && resp.data && typeof(resp.data) === 'string') {
      try {
        return JSON.parse(resp.data);
      } catch (error) {
      }
    }
    return resp.data;
  }

  public async getArrayBuffer(apiPath: string, parameters?: object): Promise<ArrayBuffer> {
    const params: AxiosRequestConfig = {
      params: parameters,
      responseType: 'arraybuffer',
      headers: {
        Accept: 'arraybuffer',
        'Content-Type': 'arraybuffer'
      }
    };

    return new Promise((res, rej) => {
      this.api
        .get(apiPath, params)
        .then(r => {
          res(r.data);
        })
        .catch(err => {
          this.errorHandler(err, rej);
        });
    });
  }

  private errorHandler = (err: any, rej: (r: any) => void) => {
    console.error('HttpError', err);
    if (!err.response) {
      rej(err);
    }

    let resp = err.response as AxiosResponse;
    rej(err);
  };

  private initAxios() {
    const requestConfig: AxiosRequestConfig = {
      baseURL: 'http://localhost:8080',
      withCredentials: false,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: (() => `Bearer ${iQVP.getToken()}`)()
      }
    };
    const _ax = axios.create(requestConfig);
    return _ax;
  }
}
