import HttpClient from './httpClient';
import { SessionHandler } from '../sessionHandler';
import DateUtils from '../utils/dateUtils';
import { ITask } from '../models/ITask';

export class TaskHandler {
    public static getTasks(dFrom: Date, dTo?: Date, done?: boolean): Promise<{ data: { tasks: ITask[]}}> {
        const user = SessionHandler.getUser();

        if (!user) {
            return new Promise(() => []);
        }

        const _dTo = dTo ? `,dateTo: "${DateUtils.dateToDateKey(dTo)}"` : '';
        const _done = done != null ? `,done:${done}` : '';
        let query = `{
            tasks(userId:${user.id}, dateFrom: "${DateUtils.dateToDateKey(dFrom)}" ${_dTo+_done}) {
                id
                title
                date
                description
                done
            }
        }`;
        return HttpClient.$.post('tasks', query);
    }

    public static removeTask(taskId: number) {
        const user = SessionHandler.getUser();
        const parameters = JSON.stringify({
            userId: user ? user.id : null,
            taskId: taskId
        });
        return HttpClient.$.post('tasks/delete', parameters)
    }

    public static createTask(task: ITask) {
        const user = SessionHandler.getUser();
        if (!user) {
            return new Promise(() => {});
        }

        const t = {
            title: task.title,
            description: task.description,
            done: task.done,
            date: Number(task.date),
            userId: user.id
        }
        return HttpClient.$.post('tasks/create', t);
    }

    public static editTask(task: ITask) {
        const user = SessionHandler.getUser();
        if (!user) {
            return new Promise(() => {});
        }

        const t = {
            id: task.id,
            title: task.title,
            description: task.description,
            done: task.done,
            date: Number(task.date),
            userId: user.id
        }
        return HttpClient.$.post('tasks/edit', t);
    }
}